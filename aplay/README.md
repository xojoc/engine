*aplay* is a hack to play sounds on GNU/Linux using the aplay command to avoid dynamic linking. You can only play and stop sounds. That's all. This covers 90% of my needs.

Example:

```
package main

import (
	"log"
	"os"
	"time"

	"gitlab.com/xojoc/engine/aplay"
)

func main() {
	f, err := os.Open("a.wav")
	if err != nil {
		log.Fatal(err)
	}
	sound := aplay.NewSound(f)
	go func() {
		err := sound.Play(-1)
		if err != nil {
			log.Fatal(err)
		}
	}()
	time.Sleep(2 * time.Second)
	sound.Stop()
}
```
see [godoc](http://godoc.org/gitlab.com/xojoc/engine/aplay) for complete documentation.
