// Written by http://xojoc.pw. Public Domain.

/*
 Package save saves and loads the state of a game.
*/
package save

import (
	"encoding/gob"
	"fmt"
	"os"
	"path"
)

var home string

func init() {
	home = os.Getenv("HOME")
}

// Path constructs the final path where to save a game's state having id id.
func Path(id string) string {
	return home + "/." + id
}

// Save saves state in the user home directory. State is encoded using encoding/gob.
// Id should be the name of the game or in the form gameCollection/gamename if it's part of
// a collection.
// The final path is something like
//     /home/user/.game
//     /home/user/.gameCollection/game
func Save(state interface{}, id string) error {
	if home == "" {
		return fmt.Errorf("HOME environment is empty")
	}
	p := Path(id)
	err := os.MkdirAll(path.Dir(p), 0755)
	if err != nil {
		return err
	}
	f, err := os.Create(p)
	if err != nil {
		return err
	}
	return gob.NewEncoder(f).Encode(state)
}

// Load loads state from the user home directory.
func Load(state interface{}, id string) error {
	if home == "" {
		// FIXME: return nil? Save already reports empty home.
		return fmt.Errorf("HOME environment is empty")
	}
	p := Path(id)
	f, err := os.Open(p)
	if err != nil {
		return err
	}
	return gob.NewDecoder(f).Decode(state)
}
