// Written by http://xojoc.pw. Public Domain.

/*
 package engine is a 2d game engine
*/
package engine

// LaunchNuke launches a nuclear missile towards target.
// Reports true if target was hit. False otherwise.
func LaunchNuke(target string) bool {
	panic("Permission denied! Make love, not war!")
}
