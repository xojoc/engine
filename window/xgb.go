// Written by http://xojoc.pw. Public domain.

package window

import (
	"encoding/binary"
	"fmt"
	"image"
	"log"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/bigreq"
	"github.com/BurntSushi/xgb/xproto"
)

/*
 The X server has to be the biggest program I’ve ever seen that doesn’t do anything for you.
   — Ken Thompson
 Programming graphics in X is like finding the square root of PI using Roman numerals.
   — Henry Spencer
 X sucks and I hate myself for writting this shit, but someone has to do it ):
   — me
 X is amazing...
   - Said nobody ever

 More quotes http://quotes.cat-v.org/programming/
*/

func put16(d []byte, u uint16) {
	binary.LittleEndian.PutUint16(d, u)
}
func put32(d []byte, u uint32) {
	binary.LittleEndian.PutUint32(d, u)
}

func (win *Window) newWidthHeight(w, h int) {
	win.chunkMaxRows = int((win.maxReqLen - uint32(win.headerLength)) / uint32(w*4))
	win.chunkMaxSize = uint64(win.headerLength + win.chunkMaxRows*w*4)
	win.pixels = make([]byte, win.headerLength+h*w*4)
	win.Screen = &image.RGBA{
		Rect:   image.Rect(0, 0, w, h),
		Pix:    win.pixels[win.headerLength:],
		Stride: 4 * w,
	}
	win.Width = w
	win.Height = h
}

func xinit(w *Window) error {
	var err error
	w.connection, err = xgb.NewConn()
	if err != nil {
		return err
	}
	setup := xproto.Setup(w.connection)
	w.minKeycode = int(setup.MinKeycode)
	screen := setup.DefaultScreen(w.connection)
	if screen.RootDepth != 24 {
		return fmt.Errorf("only 24 bitdepth supported, you have %d", screen.RootDepth)
	}
	w.window, err = xproto.NewWindowId(w.connection)
	if err != nil {
		return err
	}
	xproto.CreateWindow(w.connection, screen.RootDepth, w.window, screen.Root,
		0, 0, uint16(w.Width), uint16(w.Height), 0,
		xproto.WindowClassInputOutput, screen.RootVisual,
		xproto.CwBackPixel|xproto.CwEventMask, []uint32{
			0xffffff,
			xproto.EventMaskKeyPress |
				xproto.EventMaskKeyRelease |
				xproto.EventMaskButtonPress |
				xproto.EventMaskButtonRelease |
				xproto.EventMaskPointerMotion |
				xproto.EventMaskFocusChange |
				xproto.EventMaskStructureNotify})
	xproto.ChangeProperty(w.connection, xproto.PropModeReplace, w.window, xproto.AtomWmName, xproto.AtomString, 8, uint32(len(w.Title)), []byte(w.Title))

	protocolsCookie := xproto.InternAtom(w.connection, true, 12, "WM_PROTOCOLS")
	protocolsReply, err := protocolsCookie.Reply()
	if err != nil {
		return err
	}

	deleteCookie := xproto.InternAtom(w.connection, false, 16, "WM_DELETE_WINDOW")
	w.deleteReply, err = deleteCookie.Reply()
	if err != nil {
		return err
	}

	d := make([]byte, 4)
	put32(d, uint32(w.deleteReply.Atom))
	xproto.ChangeProperty(w.connection, xproto.PropModeReplace, w.window, protocolsReply.Atom, xproto.AtomAtom, 32, 1, d)

	xproto.MapWindow(w.connection, w.window)

	w.kmap, err = xproto.GetKeyboardMapping(w.connection, setup.MinKeycode, byte(setup.MaxKeycode-setup.MinKeycode+1)).Reply()
	if err != nil {
		return err
	}

	w.defaultGC, err = xproto.NewGcontextId(w.connection)
	if err != nil {
		return err
	}
	xproto.CreateGC(w.connection, w.defaultGC, xproto.Drawable(screen.Root),
		xproto.GcForeground, []uint32{screen.WhitePixel})

	w.maxReqLen = uint32(setup.MaximumRequestLength)

	w.headerLength = 24
	w.bigRequest = false
	err = bigreq.Init(w.connection)
	if err == nil {
		reply, err := bigreq.Enable(w.connection).Reply()
		if err == nil {
			w.maxReqLen = reply.MaximumRequestLength
			w.headerLength = 28
			w.bigRequest = true
		}
	}
	w.newWidthHeight(w.Width, w.Height)
	return nil
}

func fillChunkHeader(win *Window, chunk []byte, w uint16, h uint16, y int) {
	size := win.chunkMaxSize
	if h < uint16(win.chunkMaxRows) {
		size = uint64(win.headerLength + int(int(h)*int(w)*4))
	}

	b := 0
	// opcode
	chunk[b] = 72
	b += 1
	chunk[b] = xproto.ImageFormatZPixmap
	b += 1
	if win.bigRequest {
		// Bigrequest
		put16(chunk[b:], 0)
		b += 2
		put32(chunk[b:], uint32(size/4))
		b += 4
	} else {
		put16(chunk[b:], uint16(size/4))
		b += 2
	}
	put32(chunk[b:], uint32(win.window))
	b += 4
	put32(chunk[b:], uint32(win.defaultGC))
	b += 4
	put16(chunk[b:], w)
	b += 2
	put16(chunk[b:], h)
	b += 2
	put16(chunk[b:], 0)
	b += 2
	put16(chunk[b:], uint16(y*win.chunkMaxRows))
	b += 2
	// Left pad. Always 0 for ZPixmap
	chunk[b] = 0
	b += 1
	// Bitdepth
	chunk[b] = 24
	b += 1
	// padding
	b += 2
}

func putImage(win *Window, w uint16, h uint16, y int) {
	size := win.chunkMaxSize
	if h < uint16(win.chunkMaxRows) {
		size = uint64(win.headerLength + int(int(h)*int(w)*4))
	}

	var chunk []byte
	if y == 0 {
		chunk = win.pixels[:size]
	} else {
		low := y*win.chunkMaxRows*int(w)*4 - win.headerLength
		hig := (y*win.chunkMaxRows + int(h)) * int(w) * 4
		p := win.pixels[win.headerLength:]
		chunk = p[low:hig]
	}
	fillChunkHeader(win, chunk, w, h, y)
	cookie := win.connection.NewCookie(true, false)
	win.connection.NewRequest(chunk[:size], cookie)
	//	if int(h) != win.Height {
	err := cookie.Check()
	if err != nil {
		log.Print(err)
	}
	//	}
}

func (win *Window) xdrawImage(src *image.RGBA) {
	for y := 0; y < win.Height; y += win.chunkMaxRows {
		h := win.chunkMaxRows
		if y+win.chunkMaxRows > win.Height {
			h = win.Height - y
		}
		putImage(win, uint16(win.Width), uint16(h), y/win.chunkMaxRows)
	}
}
