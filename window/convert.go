// Written by http://xojoc.pw. Public Domain.

package window

import (
	"fmt"
	"image"
	"image/color"
	"strconv"

	"gitlab.com/xojoc/util"
)

// ColorHex parses a string in the form
// #rrggbbaa, #rrggbb, #rgba, #rgb, rrggbbaa, rrggbb, rgba or rgb
// and returns a color (in the BGRA format).
func ColorHex(hex string) (color.Color, error) {
	if len(hex) > 0 && hex[0] == '#' {
		hex = hex[1:]
	}
	ok := true
	p := func(s string) uint8 {
		if !ok {
			return 0
		}
		u, err := strconv.ParseUint(s, 16, 8)
		if err != nil {
			ok = false
		}
		return uint8(u)
	}
	var r, g, b uint8
	a := uint8(0xff)
	switch len(hex) {
	case 8:
		r = p(hex[:2])
		g = p(hex[2:4])
		b = p(hex[4:6])
		a = p(hex[6:8])
	case 6:
		r = p(hex[:2])
		g = p(hex[2:4])
		b = p(hex[4:6])
	case 4:
		r = p(hex[:1])
		r = r<<4 | r
		g = p(hex[1:2])
		g = g<<4 | g
		b = p(hex[2:3])
		b = b<<4 | b
		a = p(hex[3:4])
		a = a<<4 | a
	case 3:
		r = p(hex[:1])
		r = r<<4 | r
		g = p(hex[1:2])
		g = g<<4 | g
		b = p(hex[2:3])
		b = b<<4 | b
	default:
		ok = false
	}
	if !ok {
		return nil, fmt.Errorf("invalid hex color %q", hex)
	}
	return color.NRGBA{b, g, r, a}, nil
}

// MustColorHex is like ColorHex but calls log.Fatal if there is an error.
func MustColorHex(hex string) color.Color {
	c, err := ColorHex(hex)
	util.Fatal(err)
	return c
}

// Color converts c to a color suited for the X server (from RGBA to BGRA).
func ConvertColor(c color.Color) color.Color {
	r, g, b, a := c.RGBA()
	return color.RGBA{uint8(b >> 8), uint8(g >> 8), uint8(r >> 8), uint8(a >> 8)}
}

// ConvertImage converts img to a format suited for the X server (from RGBA to BGRA).
// NOTE: This is a slow and naive conversion, call this function only when the game loads.
func ConvertImage(img image.Image) image.Image {
	out := image.NewRGBA(img.Bounds())
	for y := img.Bounds().Min.Y; y < img.Bounds().Max.Y; y++ {
		for x := img.Bounds().Min.X; x < img.Bounds().Max.X; x++ {
			out.Set(x, y, ConvertColor(img.At(x, y)))
		}
	}
	return out
}
