// Written by http://xojoc.pw. Public Domain.

package window

// Keysyms can be found in /usr/include/X11/keysymdef.h
// FIXME: complete the list
var keysyms = map[int]string{
	0xff08: "backspace",
	0xff09: "tab",
	0xff0a: "linefeed",
	0xff0b: "clear",
	0xff0d: "return",
	0xff13: "pause",
	0xff14: "scrolllock",
	0xff15: "sysreq",
	0xff1b: "escape",
	0xffff: "delete",

	0xff51: "left",
	0xff52: "up",
	0xff53: "right",
	0xff54: "down",
}
