// Written by http://xojoc.pw. Public Domain.

/*
 Package inter provides some interpolation functions and a common API for them.
*/
package inter

import (
	"math"
)

// For a nice introduction to interpolation see http://sol.gfxile.net/interpolation/

type Map func(float64) float64

// The type of a function that returns a value between start and end as time goes from 0 to duration.
type Polation func(time, start, end, duration float64) float64

func New(f Map) Polation {
	return func(t, s, e, d float64) float64 {
		if t < 0 {
			return s
		}
		if t > d {
			return e
		}
		return s + (e-s)*f(t/d)
	}
}

func NewInv(f Map) Polation {
	return func(t, s, e, d float64) float64 {
		if t < 0 {
			return s
		}
		if t > d {
			return e
		}
		return s + (e-s)*(1-f(1-t/d))
	}
}

/*
func Clamp(p Polation) Polation {
	return func(t, s, e, d float64) float64 {
		if t < 0 {
			return s
		}
		if t > d {
			return e
		}
		return p(t, s, e, d)
	}
}
*/

// Merge returns the concatenation of one or more interpolation functions.
func Merge(funcs ...Polation) Polation {
	return func(t, b, e, d float64) float64 {
		for j := 1; j <= len(funcs); j += 1 {
			n := float64(len(funcs))
			i := float64(j)
			c := e - b
			if t < i*d/n {
				return funcs[j-1](t-((i-1)*d/n), b+(i-1)*c/n, b+i*c/n, d/n)
			}
		}
		return e
	}
}

var pow = func(p float64) Map {
	return func(zo float64) float64 { return math.Pow(zo, p) }
}

func Pow(p float64) Polation {
	return New(pow(p))
}
func PowInv(p float64) Polation {
	return NewInv(pow(p))
}

var sin = func(zo float64) float64 { return math.Sin(zo * math.Pi / 2) }
var Sin = New(sin)
var SinInv = NewInv(sin)

var smoothStep = func(zo float64) float64 { return zo * zo * (3 - 2*zo) }
var SmoothStep = New(smoothStep)
var SmoothStepInv = NewInv(smoothStep)

var smootherStep = func(zo float64) float64 { return zo * zo * zo * (zo*(zo*6-15) + 10) }
var SmootherStep = New(smootherStep)
var SmootherStepInv = NewInv(smootherStep)

/*
var exp = func(zo float64) float64 { return math.Exp(zo) / math.E }
var Exp = New(exp)
var ExpInv = NewInv(exp)

var exp2 = func(zo float64) float64 { return math.Exp2(zo) }
var Exp2 = New(exp2)
var Exp2Inv = NewInv(exp2)
*/

func catmullRom(t, p0, p1, p2, p3 float64) float64 {
	return 0.5 * ((2 * p1) +
		(-p0+p2)*t +
		(2*p0-5*p1+4*p2-p3)*t*t +
		(-p0+3*p1-3*p2+p3)*t*t*t)
}

func CatmullRom(p0, p3 float64) Polation {
	return New(func(zo float64) float64 { return catmullRom(zo, p0, 0, 1, p3) })
}

func cubicBezier(t, p0, p1, p2, p3 float64) float64 {
	return math.Pow(1-t, 3)*p0 + 3*math.Pow(1-t, 2)*t*p1 + 3*(1-t)*t*t*p2 + t*t*t*p3
}

func CubicBezier(p1, p2 float64) Polation {
	return New(func(zo float64) float64 { return cubicBezier(zo, 0, p1, p2, 1) })
}
