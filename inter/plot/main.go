// Written by http://xojoc.pw. Public Domain.

// Plot interpolation functions.
package main

import (
	"image"
	"image/color"
	"image/draw"

	"gitlab.com/xojoc/engine/inter"
	"gitlab.com/xojoc/engine/loop"
	"gitlab.com/xojoc/engine/window"
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
)

const (
	screenw = 800
	screenh = 700
)

type fun struct {
	name     string
	polation inter.Polation
}

var funcs = []fun{
	{"Pow(1)", inter.Pow(1)},
	{"Pow(2)", inter.Pow(2)},
	{"Pow(2), PowInv(2)", inter.Merge(inter.Pow(2), inter.PowInv(2))},
	{"Pow(2), PowInv(3), Pow(4)", inter.Merge(inter.Pow(2), inter.PowInv(3), inter.Pow(4))},
	{"PowInv(2)", inter.PowInv(2)},
	{"Pow(3)", inter.Pow(3)},
	{"Pow(4)", inter.Pow(4)},
	{"Pow(5)", inter.Pow(5)},
	{"Pow(6)", inter.Pow(6)},
	{"Pow(7)", inter.Pow(7)},
	{"Sin", inter.Sin},
	{"SinInv", inter.SinInv},
	/*
		{"Exp", inter.Exp},
		{"ExpInv", inter.Clamp(inter.ExpInv)},
		{"Exp2", inter.Exp},
		{"Exp2Inv", inter.ExpInv},
	*/
	{"SmoothStep", inter.SmoothStep},
	{"SmoothStepInv", inter.SmoothStepInv},
	{"SmootherStep", inter.SmootherStep},
	{"SmootherStepInv", inter.SmootherStepInv},
	{"CatmullRom(-10,1)", inter.CatmullRom(-10, 1)},
	{"CubicBezier(-1,1)", inter.CubicBezier(-1, 1)},
}

var age float64
var life = 1.0
var delayAge float64
var delay = 1.0

func update(dt float64) {
	if len(win.Keys) > 0 {
		win.Destroy()
	}
	if age >= life {
		delayAge += dt
		if delayAge > delay {
			age = 0
			delayAge = 0
		}
	} else {
		age += dt
	}
}

func render(img draw.Image) {
	w := 180
	hspace := 10
	h := 70
	vspace := 30
	maxx := screenw / (w + hspace)
	for i := 0; i < len(funcs); i++ {
		xoff := (i % maxx) * (w + hspace)
		yoff := i / maxx * (h + vspace)
		for x := 0; x < w; x++ {
			col := color.RGBA{0xff, 0x0, 0x0, 0xff}
			if float64(x) < funcs[i].polation(age, 0, float64(w), life) {
				col = color.RGBA{0x0, 0xff, 0x0, 0xff}
			}
			img.Set(xoff+x,
				yoff+(int(funcs[i].polation(float64(x), float64(h), 0, float64(w))+0.5)),
				col)

		}
		d := font.Drawer{
			Dst:  img,
			Src:  image.White,
			Face: basicfont.Face7x13,
			Dot:  fixed.P(xoff, yoff+h+15),
		}
		d.DrawString(funcs[i].name)
	}

}

var win window.Window

func main() {
	win = window.Window{
		Width:  screenw,
		Height: screenh,
		Title:  "Plot of interpolation functions",
	}
	loop.Run(&win, update, render, 30)
}
